Unreleased
==========
<!-- Append new entries here -->
* [!28](https://gitlab.com/morley-framework/morley-upgradeable/-/merge_requests/28)
  Bump morley
* [!27](https://gitlab.com/morley-framework/morley-upgradeable/-/merge_requests/27)
  Bump morley
  + Add Mumbainet support
* [!24](https://gitlab.com/morley-framework/morley-upgradeable/-/merge_requests/24)
  Update morley &c
  + ***Breaking changes***
  + Add Limanet support
  + Due to introduction of recursive lambdas, Lorentz code is not isomorphic to lamdas any more. We have to represent any storable code as lambdas now.
  + Consequently, things like entrypoint implementations and migration scripts can now technically be recursive. However, in many cases recursive lambdas are less efficient, as non-recursive ones can be concatenated, optimized, etc, while recursive ones must be pushed to the stack and executed.
  + One additional consequence is `SomePermanentImpl` is now an existential, and not a type synonym. Hence some coercions that were possible previously are now impossible. One particular effect is that upgrade parameters can't auto-derive `Eq` instances, as `SomePermanentImpl` existential is not, strictly speaking, comparable.
* [!21](https://gitlab.com/morley-framework/morley-upgradeable/-/merge_requests/21)
  Provide IsNotInView in (/==>)
* [!20](https://gitlab.com/morley-framework/morley-upgradeable/-/merge_requests/20)
  Replace `Empty` with `Never`
  + ***Breaking change***
  + `Empty` datatype is removed from newer `morley`; a compatibility module
    added in `Lorentz.Contracts.Upgradeable.Common.Empty`.
  + `emptyPermanentImpl` is set up to type error, loudly complaining about this
    change.
  + By default, permanent entrypoints are set to `Never`.
  + For legacy contracts, use `Empty` explicitly and `emptyPermanentImplCompat`.
  + For new contracts, use `Never` and `neverPermanentImpl`.
* [!19](https://gitlab.com/morley-framework/morley-upgradeable/-/merge_requests/19)
  Update to new dependencies and GHC 9
  + Temporary orphan instances missing from the last release of Lorentz added to
    `Lorentz.Contracts.Upgradeable.Common.CompatOrphans`. These orphans will be
    removed once upstream catches up.
  + The deprecated `Empty` datatype removed upstream, the definition is copied
    to `Lorentz.Contracts.Upgradeable.Common.Empty` verbatim.
* [!11](https://gitlab.com/morley-framework/morley-upgradeable/-/merge_requests/11)
  + Renamed `buildMigrationPlan` to `renderMigrationPlan`.
  + Fixed duplication in the description of `migrateExtractField` action.

0.3
======
* [!7](https://gitlab.com/morley-framework/morley-upgradeable/-/merge_requests/7)
  + Use `morley-1.14.0` and `lorentz-0.11.0`.
