# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

.PHONY: morley-upgradeable test haddock haddock-no-deps clean

# Options for development
STACK_DEV_OPTIONS = --fast --ghc-options -Wwarn --file-watch
# Options to build more stuff (tests and benchmarks)
STACK_BUILD_MORE_OPTIONS = --test --bench --no-run-tests --no-run-benchmarks
# Options for stack tests
STACK_DEV_TEST_OPTIONS = --fast --bench --no-run-benchmarks
# Options for cabal build
CABAL_BUILD_OPTIONS_WITHOUT_O = --enable-tests --enable-benchmarks
CABAL_BUILD_OPTIONS = $(CABAL_BUILD_OPTIONS_WITHOUT_O) -O0
CABAL_BENCH_OPTIONS = $(CABAL_BUILD_OPTIONS_WITHOUT_O) -O3
# Addtional (specified by user) options passed to test executable
TEST_ARGUMENTS ?=
# Packages to apply the command (build, test, e.t.c) for.
PACKAGE = morley-upgradeable
TEST_PACKAGE = morley-upgradeable-test
FULL_TEST_PACKAGE = $(PACKAGE):$(TEST_PACKAGE)

ifeq (${MORLEY_USE_CABAL},1)
	call_test = cabal new-run $(FULL_TEST_PACKAGE) $(CABAL_BUILD_OPTIONS) \
		-- --color always $(TEST_ARGUMENTS) $1
	call_nettest = cabal new-run $(FULL_TEST_PACKAGE) $(CABAL_BUILD_OPTIONS) \
		-- $(TEST_ARGUMENTS)
	call_haddock = cabal new-haddock $(CABAL_BUILD_OPTIONS) lib:$(PACKAGE)
	call_haddock_no_deps = echo "haddock-no-deps unavailable for cabal"
	call_dev = cabal new-build $(CABAL_BUILD_OPTIONS)
	call_clean = cabal new-clean
else
	call_test = stack test $(FULL_TEST_PACKAGE) $(STACK_DEV_TEST_OPTIONS) \
		--test-arguments "--color always $(TEST_ARGUMENTS) $1"
	call_nettest = stack test $(FULL_TEST_PACKAGE) $(STACK_DEV_TEST_OPTIONS) \
		--test-arguments "$(TEST_ARGUMENTS)"
	call_haddock = stack haddock $(STACK_DEV_OPTIONS) $(PACKAGE)
	call_haddock_no_deps = stack haddock $(STACK_DEV_OPTIONS) $(PACKAGE) --no-haddock-deps
	call_dev = stack build $(STACK_DEV_OPTIONS) $(STACK_BUILD_MORE_OPTIONS) $(PACKAGE)
	call_clean = stack clean $(PACKAGE)
endif

morley-upgradeable:
	$(call call_dev,"")
test:
	$(call call_test,)
test-dumb-term:
	TERM=dumb $(call call_test,)
test-hide-successes:
	TERM=dumb $(call call_test,"--hide-successes")
haddock:
	$(call call_haddock,"")
haddock-no-deps:
	$(call call_haddock_no_deps,"")
clean:
	$(call call_clean,"")
stylish:
	find app/ src/ test/ -name '.stack-work' -prune -o -name '*.hs' -exec stylish-haskell -i '{}' \;
