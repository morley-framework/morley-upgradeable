> :warning: **Note: this project is deprecated.**
>
> It is no longer maintained since the activation of protocol "Nairobi" on the Tezos mainnet (June 24th, 2023).

# Morley Upgradeable

This package contains infrastructure that one can use to implement an upgradeable contract.
For overview of upgradeability approaches please read [the related document](/docs/upgradeableContracts.md).
