<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Scripts

Here we store scripts for various purposes.
Brief overview is provided below.

## Development

* `lint.hs` calls `hlint` for source files with proper arguments.

## CI

Basically CI performs the following steps:

* Build haskell code and run its tests using stack.
* Perform `hlint`, `weeder`, `xrefcheck` and some other code quality checks.
