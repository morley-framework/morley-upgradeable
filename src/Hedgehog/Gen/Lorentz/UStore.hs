-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.Lorentz.UStore
  ( genUStoreSubMap
  , genUStoreFieldExt
  ) where

import Prelude

import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Lorentz.UStore.Types (UStoreFieldExt(..), type (|~>)(..))

import Test.Cleveland.Util (genTuple2)

genUStoreSubMap :: (MonadGen m, Ord k) => m k -> m v -> m (k |~> v)
genUStoreSubMap genK genV = UStoreSubMap <$> Gen.map (Range.linear 0 100) (genTuple2 genK genV)

genUStoreFieldExt :: MonadGen m => m v -> m (UStoreFieldExt marker v)
genUStoreFieldExt genV = UStoreField <$> genV
