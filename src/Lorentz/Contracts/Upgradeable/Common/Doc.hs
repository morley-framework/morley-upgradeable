-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Lorentz.Contracts.Upgradeable.Common.Doc
  ( DUpgradeability (..)
  , UpgradeableEntrypointsKind
  , contractDoc
  , runDoc
  , runPermDoc
  , upgradeDoc
  , getVersionDoc
  , setAdministratorDoc
  , epwBeginUpgradeDoc
  , epwApplyMigrationDoc
  , epwSetCodeDoc
  , epwSetPermCodeDoc
  , epwFinishUpgradeDoc
  ) where

import Lorentz

import Fmt (Buildable(build))

import Morley.Util.Markdown

data DUpgradeability = DUpgradeability Markdown

instance DocItem DUpgradeability where
  docItemPos = 112
  docItemSectionName = Just "Contract upgradeability"
  docItemToMarkdown _ (DUpgradeability txt) = build txt

-- | Common marker for upgradeable, or /virtual/, entrypoints.
-- Can be used when each upgradeable entrypoint is simple,
-- i.e. does not itself consist of multiple entrypoints.
data UpgradeableEntrypointsKind

instance EntrypointKindHasDoc UpgradeableEntrypointsKind where
  entrypointKindPos = 1050
  entrypointKindSectionName = "Top-level entrypoints of upgradeable contract"
  entrypointKindSectionDescription = Just
    "These entrypoints may change in new versions of the contract.\n\n\
    \Also they have a special calling routing, see the respective subsection \
    \in every entrypoint description."


contractDoc :: Markdown
contractDoc = [md|
  This contract uses upgradeability approach described [here](https://gitlab.com/morley-framework/morley/-/blob/a30dddb633ee880761c3cbf1d4a69ee040ffad25/docs/upgradeableContracts.md#section-2-administrator-forced-upgrades).
  This mechanism provides adminstrator-forced address-preserving upgradeability
  approach. For more information check out the doc referenced earlier.
  |]

runDoc :: Markdown
runDoc =
  "This entrypoint extracts contract code kept in storage under the \
  \corresponding name and executes it on an argument supplied via `UParam`."

runPermDoc :: Markdown
runPermDoc =
  "Similar to `Run` entrypoint, but calls permanent entrypoints - ones \
  \that will be present in all versions of the contract."

upgradeDoc :: Markdown
upgradeDoc = [md|
  This entry point is used to update the contract to a new version.
  Consider using this entrypoint when your upgrade to the new version isn't very large,
  otherwise, transaction with this entrypoint call won't fit instruction size limit.
  If this is your case, consider using entrypoint-wise upgrade. This entrypoint
  basically exchange `code` field in the storage and upgrade `dataMap` using
  provided migration lambda.
  |]

getVersionDoc :: Markdown
getVersionDoc =
  "This entry point is used to get contract version."

setAdministratorDoc :: Markdown
setAdministratorDoc =
  "This entry point is used to set the administrator address."

epwBeginUpgradeDoc :: Markdown
epwBeginUpgradeDoc =
  "This entry point is used to start an entrypoint wise upgrade of the contract."

epwApplyMigrationDoc :: Markdown
epwApplyMigrationDoc =
  "This entry point is used to apply a storage migration script as part of an upgrade."

epwSetCodeDoc :: Markdown
epwSetCodeDoc =
  "This entry point is used to set the dispatching code that calls the packed entrypoints."

epwSetPermCodeDoc :: Markdown
epwSetPermCodeDoc =
  "Similar to `EpwSetCode`, but refers to permanent entrypoints - ones \
  \that will be present in all versions of the contract."

epwFinishUpgradeDoc :: Markdown
epwFinishUpgradeDoc =
  "This entry point is used to mark that an upgrade has been finsihed."
