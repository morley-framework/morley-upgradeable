-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Lorentz.Contracts.Upgradeable.Test
  ( -- * Test predicates
    testUpgradeableContractDoc

    -- * Individual test predicates
  , testSingleVersion
  ) where

import Prelude

import Test.HUnit (assertFailure)

import Lorentz.Contracts.Upgradeable.Common
import Test.Cleveland.Doc.Lorentz

-- | Check that contract documentation mentions version only once.
testSingleVersion :: DocTest
testSingleVersion =
  mkDocTest "Version documented" $
  \doc ->
    case allContractDocItems @DVersion doc of
      [] -> assertFailure "Contract contains no 'DVersion'"
      [_] -> pass
      _ -> assertFailure "Contract contains several 'DVersion's"

-- | Test all properties of upgradeable contract.
testUpgradeableContractDoc :: [DocTest]
testUpgradeableContractDoc = mconcat
  [ testLorentzDoc
  , [ testSingleVersion
    ]
  ]
