-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

module Lorentz.UStore.Instances
  ( ustoreFieldOps
  , ustoreSubmapOps
  ) where

import Prelude (Typeable, (.))

import Lorentz.StoreClass
import Lorentz.UStore.Instr
import Lorentz.UStore.Types

ustoreFieldOps
  :: (HasUField fname ftype templ, Typeable templ)
  => StoreFieldOps (UStore templ) fname ftype
ustoreFieldOps =
  StoreFieldOps
  { sopToField = ustoreToField . fieldNameToLabel
  , sopGetFieldOpen = \cont1 cont2 -> ustoreGetFieldOpen cont1 cont2 . fieldNameToLabel
  , sopSetFieldOpen = \cont -> ustoreSetFieldOpen cont . fieldNameToLabel
  }

instance (HasUField fname ftype templ, Typeable templ) =>
         StoreHasField (UStore templ) fname ftype where
  storeFieldOps = ustoreFieldOps

ustoreSubmapOps
  :: HasUStore mname key value templ
  => StoreSubmapOps (UStore templ) mname key value
ustoreSubmapOps = StoreSubmapOps
  { sopMem = ustoreMem . fieldNameToLabel
  , sopGet = ustoreGet . fieldNameToLabel
  , sopUpdate = ustoreUpdate . fieldNameToLabel
  , sopGetAndUpdate = ustoreGetAndUpdate . fieldNameToLabel
  , sopDelete = ustoreDelete . fieldNameToLabel
  , sopInsert = ustoreInsert . fieldNameToLabel
  }

instance {-# OVERLAPPING #-}
         HasUStore mname key value templ =>
         StoreHasSubmap (UStore templ) mname key value where
  storeSubmapOps = ustoreSubmapOps
