-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests on ordering of documentation items.
module Test.Doc.Positions
  ( test_Errors
  ) where

import Test.Tasty (TestTree)

import Lorentz.Contracts.Upgradeable.Common.Base
  (PermanentEntrypointsKind, UpgradeableEntrypointsKind)
import Lorentz.Entrypoints.Doc (DEntrypoint, PlainEntrypointsKind)
import Test.Cleveland.Util (goesBefore)

test_Errors :: [TestTree]
test_Errors =
  [ Proxy @(DEntrypoint PlainEntrypointsKind) `goesBefore` Proxy @(DEntrypoint PermanentEntrypointsKind)
  , Proxy @(DEntrypoint PermanentEntrypointsKind) `goesBefore` Proxy @(DEntrypoint UpgradeableEntrypointsKind)
  ]
