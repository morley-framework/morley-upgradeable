-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Lorentz.Contracts.UpgradeableCounter
  ( test_UpgradeableCounter
  ) where

import Lorentz (VoidResult(..), mkVoid, (#))
import Lorentz qualified as L

import Data.Coerce (coerce)
import Test.Tasty (TestTree, testGroup)

import Lorentz.Constraints
import Lorentz.Contracts.Upgradeable.Common
import Lorentz.Contracts.UpgradeableCounter
import Lorentz.Contracts.UpgradeableCounter.V1 qualified as V1
import Lorentz.Contracts.UpgradeableCounter.V2 qualified as V2
import Lorentz.UParam
import Lorentz.UStore
import Lorentz.Value
import Morley.Util.Instances ()
import Test.Cleveland
import Test.Cleveland.Lorentz.Consumer

{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

originateUpgradeableCounter
  :: MonadCleveland caps m => ImplicitAddressWithAlias -> m (UTAddress CounterV0)
originateUpgradeableCounter admin =
  toL1TAddress <$> originate
    "UpgradeableCounter"
    (mkEmptyStorage admin)
    upgradeableCounterContract
    [tz|1000u|]

originateUpgradeableCounterV1
  :: MonadCleveland caps m => ImplicitAddressWithAlias -> m (UTAddress V1.CounterV1)
originateUpgradeableCounterV1 admin = do
  contract <- originateUpgradeableCounter admin
  withSender admin $ upgradeToV1 contract
  return (coerce contract)

-- We deliberately use forall here so that we can test incorrect upgrades
upgradeToV1
  :: MonadCleveland caps m
  => UTAddress sign
  -> m (UTAddress V1.CounterV1)
upgradeToV1 =
  integrationalTestUpgrade V1.counterUpgradeParameters UpgEntrypointWise
  . coerce

-- We deliberately use forall here so that we can test incorrect upgrades
upgradeToV2
  :: MonadCleveland caps m
  => UTAddress sign
  -> m (UTAddress V2.CounterV2)
upgradeToV2 =
  integrationalTestUpgrade V2.counterUpgradeParameters UpgEntrypointWise
  . coerce

uCall
  :: forall a name (ver :: VersionKind) (interface :: [EntrypointKind]) caps m.
  ( interface ~ VerInterface ver
  , NicePackedValue a
  , RequireUniqueEntrypoints interface
  , PermConstraint ver
  , LookupEntrypoint name interface ~ a
  , MonadCleveland caps m
  )
  => UTAddress ver
  -> Label name
  -> a
  -> m ()
uCall contract method arg = do
  transfer contract $ calling def $ Run ((mkUParam method arg) :: UParam interface)

getCounterValueV1
  :: MonadCleveland caps m
  => UTAddress V1.CounterV1
  -> m ()
getCounterValueV1 contract = do
  uCall contract #getCounterValue $ mkVoid ()

getVersion
  :: (PermConstraint ver, MonadCleveland caps m)
  => UTAddress ver
  -> TAddress Version ()
  -> m ()
getVersion contract consumer = do
  transfer contract $ calling def $ GetVersion (L.mkView_ () consumer)

getCounterValueV2
  :: MonadCleveland caps m
  => UTAddress V2.CounterV2
  -> m ()
getCounterValueV2 contract = do
  uCall contract #getCounterValue $ mkVoid ()

test_UpgradeableCounter :: [TestTree]
test_UpgradeableCounter =
  [ testGroup "v1"
    [ testScenario "Initially contains zero" $ scenario do
        admin <- newAddress "admin"
        contract <- originateUpgradeableCounterV1 admin
        getCounterValueV1 contract & expectError
          (VoidResult @Natural 0)

    , testScenario "Updates counter after each operation" $ scenario do
        admin <- newAddress "admin"
        contract <- originateUpgradeableCounterV1 admin

        uCall contract #add (2 :: Natural)
        uCall contract #mul (3 :: Natural)
        getCounterValueV1 contract & expectError
          (VoidResult @Natural 6)
    ]
  , testGroup "v2"
    [ testScenario "Upgrades to v2" $ scenario do
        admin <- newAddress "admin"
        contract <- originateUpgradeableCounterV1 admin
        consumer <- originate "consumer" [] contractConsumer

        getVersion contract $ toTAddress consumer
        contract2 <- withSender admin $ upgradeToV2 contract
        getVersion contract2 $ toTAddress consumer

        getStorage consumer @@== reverse [1, 2]
        -- lExpectViewConsumerStorage consumer [1, 2]

    , testScenario "Preserves the counter after the upgrade" $ scenario do
        admin <- newAddress "admin"
        contract <- originateUpgradeableCounterV1 admin

        uCall contract #add (42 :: Natural)
        contract2 <- withSender admin $ upgradeToV2 contract
        getCounterValueV2 contract2 & expectError
          (VoidResult @Integer 42)

    , testScenarioOnEmulator "Exposes new methods" $ scenarioEmulated do
        admin <- newAddress "admin"
        contract <- originateUpgradeableCounterV1 admin

        contract2 <- withSender admin $ upgradeToV2 contract
        uCall contract2 #inc ()
        uCall contract2 #inc ()

        branchout
          [ "2 inc" ?-
              getCounterValueV2 contract2 & expectError
                (VoidResult @Integer 2)

          , "2 inc, 1 dec" ?- do
              uCall contract2 #dec ()
              getCounterValueV2 contract2 & expectError
                (VoidResult @Integer 1)
          ]

    , testScenario "Allows to decrement below zero" $ scenario do
        admin <- newAddress "admin"
        contract <- originateUpgradeableCounterV1 admin

        contract2 <- withSender admin $ upgradeToV2 contract
        uCall contract2 #dec ()
        uCall contract2 #dec ()
        uCall contract2 #dec ()
        getCounterValueV2 contract2 & expectError
          (VoidResult @Integer (-3))
    ]
  , testGroup "Cross-version"
    [ testScenario "Can migrate to the same version" $ scenario do
        admin <- newAddress "admin"
        contract <- originateUpgradeableCounterV1 admin
        void $ withSender admin $
          transfer contract $ calling def $ makeOneShotUpgrade
          ( fvUpgrade . migrationToScriptI . mkUStoreMigration $
                L.push 100 # migrateModifyField #counterValue
          )
    ]

  , testGroup "Illegal migrations"
    [ testScenario "Cannot migrate if sender is not admin" $ scenario do
        admin <- newAddress "admin"
        adversary <- newAddress "adversary"
        contract <- originateUpgradeableCounter admin
        withSender adversary (upgradeToV1 contract) & expectCustomError
          #senderIsNotAdmin_ ()
    ]

  , testGroup "Administrator change"
    [ testScenario "Admin can set a new administrator" $ scenario do
        admin <- newAddress "admin"
        admin2 <- newAddress "admin2"
        contract <- originateUpgradeableCounter admin
        withSender admin . transfer contract $ calling def $ SetAdministrator (toAddress admin2)
        void $ withSender admin2 $ upgradeToV1 contract

    , testScenario "Non-admin cannot set a new administrator" $ scenario do
        admin <- newAddress "admin"
        admin2 <- newAddress "admin2"
        adversary <- newAddress "adversary"
        contract <- originateUpgradeableCounter admin
        withSender adversary (transfer contract $ calling def $ SetAdministrator (toAddress admin2))
          & expectCustomError #senderIsNotAdmin_ ()
    ]
  ]
