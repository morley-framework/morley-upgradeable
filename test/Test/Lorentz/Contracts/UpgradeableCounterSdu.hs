-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Lorentz.Contracts.UpgradeableCounterSdu
  ( test_UpgradeableCounterSdu
  , test_Documentation
  ) where

import Lorentz (VoidResult(..), mkView_, mkVoid)

import Data.Coerce (coerce)
import Fmt (pretty)
import Test.Tasty (TestTree, testGroup)

import Lorentz.Constraints
import Lorentz.Contracts.Upgradeable.Common
import Lorentz.Contracts.Upgradeable.Test
import Lorentz.Contracts.UpgradeableCounterSdu
import Lorentz.Contracts.UpgradeableCounterSdu.V1 qualified as V1
import Lorentz.Contracts.UpgradeableCounterSdu.V2 qualified as V2
import Lorentz.UParam
import Lorentz.Value
import Morley.Util.Instances ()
import Morley.Util.Named
import Test.Cleveland
import Test.Cleveland.Doc.Lorentz
import Test.Cleveland.Lorentz.Consumer

{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

originateUpgradeableCounter
  :: MonadCleveland caps m => ImplicitAddressWithAlias -> m (UTAddress (CounterSduV 0))
originateUpgradeableCounter admin =
  toL1TAddress <$> originate
    "UpgradeableCounter"
    (mkEmptyStorage admin)
    upgradeableCounterContractSdu
    [tz|1000u|]

originateUpgradeableCounterV1
  :: MonadCleveland caps m => ImplicitAddressWithAlias -> m (UTAddress (CounterSduV 1))
originateUpgradeableCounterV1 admin = do
  contract <- originateUpgradeableCounter admin
  withSender admin $ upgradeToV1 UpgEntrypointWise contract

upgradeToV1
  :: MonadCleveland caps m
  => SimpleUpgradeWay
  -> UTAddress (CounterSduV 0)
  -> m (UTAddress (CounterSduV 1))
upgradeToV1 = integrationalTestUpgrade V1.counterUpgradeParameters

upgradeToV2
  :: MonadCleveland caps m
  => SimpleUpgradeWay
  -> UTAddress (CounterSduV 1)
  -> m (UTAddress (CounterSduV 2))
upgradeToV2 = integrationalTestUpgrade V2.counterUpgradeParameters

upgradeV0ToV2
  :: MonadCleveland caps m
  => SimpleUpgradeWay
  -> UTAddress (CounterSduV 0)
  -> m (UTAddress (CounterSduV 2))
upgradeV0ToV2 = integrationalTestUpgrade V2.counterUpgradeParametersFromV0

uCall
  :: forall a name (ver :: VersionKind) (interface :: [EntrypointKind]) caps m.
  ( interface ~ VerInterface ver
  , NicePackedValue a
  , PermConstraint ver
  , RequireUniqueEntrypoints interface
  , LookupEntrypoint name interface ~ a
  , MonadCleveland caps m
  )
  => UTAddress ver
  -> Label name
  -> a
  -> m ()
uCall contract method arg' = do
  transfer contract $ calling def $ Run ((mkUParam method arg') :: UParam interface)

getCounterValueV1
  :: UTAddress (CounterSduV 1)
  -> MonadCleveland caps m => m ()
getCounterValueV1 contract = do
  uCall contract #epGetCounterValue $ mkVoid ()

getCounterValueV2
  :: UTAddress (CounterSduV 2)
  -> MonadCleveland caps m => m ()
getCounterValueV2 contract = do
  uCall contract #epGetCounterValue $ mkVoid ()

test_UpgradeableCounterSdu :: [TestTree]
test_UpgradeableCounterSdu =
  (<$> [UpgOneShot, UpgEntrypointWise]) $ \upgWay ->
  testGroup (pretty upgWay)
    -- Most of the logic is covered in tests for similar 'UpgradeableCounter'
    -- contract (with entrypoint-wise migration way), not including such tests
    -- here, only ones on the main functionality
    [ testGroup "v1"
      [ testScenario "Updates counter after each operation" $ scenario do
          admin <- newAddress "admin"
          contract <- originateUpgradeableCounterV1 admin

          uCall contract #epAdd (2 :: Natural)
          uCall contract #epInc ()
          getCounterValueV1 contract & expectError
            (VoidResult @Natural 3)

      , testScenario "Can call permanent entrypoint" $ scenario do
          admin <- newAddress "admin"
          contract <- originateUpgradeableCounterV1 admin

          uCall contract #epAdd (5 :: Natural)
          expectError (VoidResult @Integer 5) $
            transfer contract $ calling (ep @"GetCounter") (mkVoid ())
      ]

    , testGroup "v2"
      [ testScenarioOnEmulator "Upgrade and further operations work fine" $ scenarioEmulated do
          admin <- newAddress "admin"
          contract <- originateUpgradeableCounterV1 admin

          uCall contract #epAdd (2 :: Natural)

          offshoot "Before migration" $ do
            getCounterValueV1 contract & expectError
              (VoidResult @Natural 2)

          contract2 <- withSender admin $ upgradeToV2 upgWay contract

          offshoot "Right after migration" $ do
            getCounterValueV2 contract2 & expectError
              (VoidResult @Integer 2)

          offshoot "Cannot call removed entrypoint" $ do
            uCall contract #epAdd (5 :: Natural) & expectCustomError
              #uparamNoSuchEntrypoint [mt|epAdd|]

          uCall contract2 #epDec ()

          offshoot "After dec" $ do
            getCounterValueV2 contract2 & expectError
              (VoidResult @Integer 1)

      , testScenarioOnEmulator "Upgrade from scratch works fine" $ scenarioEmulated do
          admin <- newAddress "admin"
          contractV0 <- originateUpgradeableCounter admin
          contract <- withSender admin $ upgradeV0ToV2 upgWay contractV0

          branchout
            [ "Can call operations" ?- do
                uCall contract #epInc ()
                uCall contract #epDec ()

            , "Version field has expected value" ?- do
                consumer <- originate "consumer" [] contractConsumer
                transfer contract $ calling (ep @"GetVersion") $ mkView_ () consumer
                getStorage consumer @@== [2]
            ]

      , testScenario "Can decrease version" $ scenario do
          admin <- newAddress "admin"
          contractV2 <- originateUpgradeableCounterV1 admin >>=
                        withSender admin . upgradeToV2 upgWay

          contractV1 <-
            withSender admin $
              integrationalTestUpgrade V2.counterRollbackParameters upgWay contractV2

          consumer <- originate "consumer" [] contractConsumer

          transfer contractV1 $ calling def $ GetVersion (mkView_ () consumer)

          getStorage consumer @@== [1]

      , testScenario "Fails if wrong old version is provided" $ scenario do
          admin <- newAddress "admin"
          contract <- originateUpgradeableCounterV1 admin
          withSender admin (upgradeV0ToV2 upgWay (coerce contract)) & expectCustomError
            #upgVersionMismatch (#expectedCurrent :! 0, #actualCurrent :! 1)

      , testScenario "Can call permanent entrypoint" $ scenario do
          admin <- newAddress "admin"
          contract <- originateUpgradeableCounterV1 admin
          uCall contract #epAdd (5 :: Natural)

          contract2 <- withSender admin $ upgradeToV2 upgWay contract
          uCall contract2 #epInc ()
          expectError (VoidResult @Integer 6) $
            transfer contract2 $ calling def $ RunPerm $ GetCounter $ mkVoid ()
      ]
    ]

test_Documentation :: [TestTree]
test_Documentation =
  runDocTests testSuites V1.counterDoc
  where
    testSuites =
      testUpgradeableContractDoc `excludeDocTests`
      [ testEachEntrypointIsDescribed
      ]
