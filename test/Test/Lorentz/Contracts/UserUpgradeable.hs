-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for user-defined upgrades

module Test.Lorentz.Contracts.UserUpgradeable
  ( test_UserUpgradeable
  ) where

import Test.Tasty (TestTree)

import Lorentz (mkView_)
import Lorentz.Contracts.UserUpgradeable.Migrations (MigrationTarget)
import Lorentz.Contracts.UserUpgradeable.V1 qualified as V1
import Lorentz.Contracts.UserUpgradeable.V2 qualified as V2
import Lorentz.Value
import Test.Cleveland
import Test.Cleveland.Lorentz.Consumer
import Test.Cleveland.Lorentz.Types

{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

originateV1
  :: (MonadCleveland caps m, ToAddress addr)
  => ImplicitAddressWithAlias -> addr -> m (L1TAddress V1.Parameter ())
originateV1 admin wallet1 =
  toL1TAddress <$> originate
    "UserUpgradeable V1"
    (V1.mkStorage balances $ toImplicitAddress admin)
    V1.userUpgradeableContract
    [tz|1000u|]
  where
    balances = mkBigMap
      [ (toAddress wallet1, 100)
      ]

originateV2
  :: L1TAddress V1.Parameter () -> MonadCleveland caps m
  => m (L1TAddress V2.Parameter ())
originateV2 prevVersion =
  toL1TAddress <$> originate
    "UserUpgradeable V2"
    (V2.mkStorage $ toTAddress prevVersion)
    V2.userUpgradeableContract
    [tz|1000u|]

-- | A helper function that originates v1 and v2, and initiates an upgrade
-- from v1 to v2.
initMigration
  :: MonadCleveland caps m
  => ImplicitAddressWithAlias -> ImplicitAddressWithAlias
  -> m (L1TAddress V1.Parameter (), L1TAddress V2.Parameter ())
initMigration admin wallet1 = do
  v1 <- originateV1 admin wallet1
  v2 <- originateV2 v1
  withSender admin $
    transfer v1 $ calling def $ V1.InitiateMigration (migrateFromEntrypoint v2)
  return (v1, v2)

migrateMyTokens
  :: L1TAddress V1.Parameter () -> ImplicitAddressWithAlias -> Natural
  -> MonadCleveland caps m => m ()
migrateMyTokens v1 wallet amount = do
  withSender wallet $
    transfer v1 $ calling def $ V1.MigrateMyTokens amount

migrateFromEntrypoint :: L1TAddress V2.Parameter () -> MigrationTarget
migrateFromEntrypoint c =
  fromContractRef . callingAddress c $ Call @"MigrateFrom"

test_UserUpgradeable :: [TestTree]
test_UserUpgradeable =
  [ testScenario "Arbitrary user can not initiate an upgrade" $ scenario do
        admin <- newAddress "admin"
        wallet1 <- newFreshAddress auto
        v1 <- originateV1 admin wallet1
        v2 <- originateV2 v1
        expectCustomError_ #senderIsNotAdmin_ $
          transfer v1 $ calling def (V1.InitiateMigration (migrateFromEntrypoint v2))

  , testScenario "Cannot initiate an upgrade twice" $ scenario do
        admin <- newAddress "admin"
        wallet1 <- newFreshAddress auto
        (v1, v2) <- initMigration admin wallet1
        withSender admin
          (transfer v1 $ calling def $ V1.InitiateMigration (migrateFromEntrypoint v2))
          & expectCustomError_ #alreadyMigrating

  , testScenario "Cannot call migrate if the migration is not initiated" $ scenario do
        admin <- newAddress "admin"
        wallet1 <- newAddress "wallet1"
        v1 <- originateV1 admin wallet1
        migrateMyTokens v1 wallet1 100
          & expectCustomError_ #nowhereToMigrate

  , testScenario "Migrations burn old tokens" $ scenario do
        admin <- newAddress "admin"
        wallet1 <- newAddress "wallet1"
        (v1, _) <- initMigration admin wallet1
        consumer <- originate "consumer" [] $ contractConsumer @Natural

        migrateMyTokens v1 wallet1 90
        migrateMyTokens v1 wallet1 9

        transfer v1 $ calling def $ V1.GetBalance (mkView_ (toAddress wallet1) consumer)
        getStorage consumer @@== [1]

  , testScenario "Can migrate the whole balance" $ scenario do
        admin <- newAddress "admin"
        wallet1 <- newAddress "wallet1"
        (v1, _) <- initMigration admin wallet1
        consumer <- originate "consumer" [] $ contractConsumer @Natural

        migrateMyTokens v1 wallet1 100

        transfer v1 $ calling def $ V1.GetBalance (mkView_ (toAddress wallet1) consumer)
        getStorage consumer @@== [0]

  , testScenario "Cannot migrate more than you have" $ scenario do
        admin <- newAddress "admin"
        wallet1 <- newAddress "wallet1"
        (v1, _) <- initMigration admin wallet1

        migrateMyTokens v1 wallet1 101
          & expectCustomError_ #userUpgradable'notEnoughTokens

  , testScenario "Migrations mint new tokens" $ scenario do
        admin <- newAddress "admin"
        wallet1 <- newAddress "wallet1"
        (v1, v2) <- initMigration admin wallet1
        consumer <- originate "consumer" [] $ contractConsumer @Natural

        migrateMyTokens v1 wallet1 90
        migrateMyTokens v1 wallet1 9

        transfer v2 $ calling def $ V2.GetBalance (mkView_ (toAddress wallet1) consumer)
        getStorage consumer @@== [99]

  , testScenario "Cannot call MigrateFrom directly" $ scenario do
        admin <- newAddress "admin"
        wallet1 <- newAddress "wallet1"
        (_, v2) <- initMigration admin wallet1

        withSender wallet1
          (transfer v2 $ calling def $ V2.MigrateFrom (toAddress wallet1, 100))
          & expectCustomError_ #userUpgradable'unauthorizedMigrateFrom
  ]
