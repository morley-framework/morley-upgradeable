-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Lorentz.UStore.Migration.Batched.V1
  ( MyTemplate (..)
  ) where

import Lorentz.Lambda
import Lorentz.UStore

data MyTemplate = MyTemplate
  { bytes :: Integer |~> ByteString
  , int1 :: UStoreField Integer
  , int2 :: UStoreField Integer
  , code1 :: UStoreField $ Lambda () ()
  , code2 :: UStoreField $ Lambda Natural Integer
  , code3 :: UStoreField $ Lambda () ()
  } deriving stock Generic
