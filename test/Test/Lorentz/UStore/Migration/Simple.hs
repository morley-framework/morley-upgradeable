-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Lorentz.UStore.Migration.Simple
  ( test_Migration_works
  ) where

import Test.HUnit (assertFailure, (@?=))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Lorentz qualified as L
import Lorentz.Run.Simple
import Lorentz.UStore
import Lorentz.UStore.Migration
import Lorentz.UStore.Migration.Diff
import Morley.Michelson.Text

import Test.Lorentz.UStore.Migration.Simple.V1 qualified as V1
import Test.Lorentz.UStore.Migration.Simple.V2 qualified as V2

_checkDiff :: Proxy (BuildDiff V1.MyTemplate V2.MyTemplate)
_checkDiff = Proxy @(
  [ '( 'ToAdd, '("theName", UStoreField MText))
  , '( 'ToAdd, '("transformed", UStoreField Integer))
  , '( 'ToDel, '("useless", UStoreField MText))
  , '( 'ToDel, '("transformed", UStoreField Natural))
  ])

migrationBatched :: UStoreMigration V1.MyTemplate V2.MyTemplate
migrationBatched = mkUStoreBatchedMigration $
  muBlock $:
    migrateExtractField #useless L.#
    L.push [mt|Token-|] L.#
    L.concat L.#
    migrateAddField #theName
  <-->
  muBlock $:
    L.push 3 L.#
    migrateOverwriteField #transformed
  <-->
  migrationFinish

migrationSimple :: UStoreMigration V1.MyTemplate V2.MyTemplate
migrationSimple = mkUStoreMigration $
  migrateExtractField #useless L.#
  L.push [mt|Token-|] L.#
  L.concat L.#
  migrateAddField #theName L.#

  L.push 3 L.#
  migrateOverwriteField #transformed L.#

  migrationFinish

test_Migration_works :: [TestTree]
test_Migration_works =
  [ ("simple migration", migrationSimple)
  , ("batched migration", migrationBatched)
  ] <&> \(desc, migration) ->
    testCase desc $ migratesWith migration
      V1.MyTemplate
      { V1.bytes = UStoreSubMap mempty
      , V1.count = UStoreField 5
      , V1.useless = UStoreField [mt|pog|]
      , V1.transformed = UStoreField 10
      }
      V2.MyTemplate
      { V2.theName = UStoreField [mt|Token-pog|]
      , V2.bytes = UStoreSubMap mempty
      , V2.count = UStoreField 5
      , V2.transformed = UStoreField 3
      }
  where
    migratesWith migration storeV1 expectedStoreV2 =
      either (assertFailure . toString) (@?= expectedStoreV2) $ do
        let storeV2 = migrationToLambda migration -$ mkUStore storeV1
        ustoreDecomposeFull storeV2
