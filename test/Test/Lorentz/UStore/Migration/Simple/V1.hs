-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Lorentz.UStore.Migration.Simple.V1
  ( MyTemplate (..)
  ) where

import Lorentz.UStore
import Lorentz.Value

data MyTemplate = MyTemplate
  { bytes :: Integer |~> ByteString
  , count :: UStoreField Integer
  , useless :: UStoreField MText
  , transformed :: UStoreField Natural
  } deriving stock Generic
