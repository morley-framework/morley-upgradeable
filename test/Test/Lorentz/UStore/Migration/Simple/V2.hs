-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Lorentz.UStore.Migration.Simple.V2
  ( MyTemplate (..)
  ) where

import Lorentz.UStore
import Lorentz.Value

data MyTemplate = MyTemplate
  { theName :: UStoreField MText
  , bytes :: Integer |~> ByteString
  , count :: UStoreField Integer
  , transformed :: UStoreField Integer
  } deriving stock (Eq, Show, Generic)
